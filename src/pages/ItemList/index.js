import React, { useEffect } from "react";
import CardContainer from "../../components/CardContainer";
import { getItems } from "../../redux/actions/items";
import { useDispatch, useSelector } from "react-redux";
import {
  Grid,
  Button,
  LinearProgress,
} from "@material-ui/core";
import { itemsResult } from "../../redux/selectors";
import styles from "./style";
import { JumpTo } from "../../utils/JumpTo";
import ItemCard from "../../components/ItemCard";

export const ItemList = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const itemsResponse = useSelector(state => itemsResult(state));

  useEffect(() => {
    dispatch(getItems());
  }, [dispatch]);

  console.log(itemsResponse);
  
  const renderItems = () => {
    return itemsResponse === undefined ? (
      <LinearProgress size={50} color="primary" />
    ) : (
      itemsResponse.map((item, index) => {
        return (
          <ItemCard key={index+1} name={item.name} url={item.url} />
        )
      })
    );
  };

  return (
    <Grid className={classes.gridContainer}>
      <CardContainer>
        {renderItems()}
      </CardContainer>
      <Button variant="contained" color="secondary" onClick={JumpTo("/")}>
        To Home
      </Button>
    </Grid>
  );
};

export default ItemList;
