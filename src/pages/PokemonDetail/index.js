import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPokemonData } from "../../redux/actions/pokemons";
import { pokemonDataResult, pkmIsLoading } from "../../redux/selectors";
import CardContainer from "../../components/CardContainer";
import { CircularProgress } from "@material-ui/core";
import PokemonDetailComponent from "../../components/PokemonDetailComponent";
import { useParams } from "react-router";

export const PokemonDetail = () => {
  const {name} = useParams();
  const dispatch = useDispatch();
  const pokemon = useSelector(state => pokemonDataResult(state));
  const isLoading = useSelector(state => pkmIsLoading(state));

  useEffect(() => {
    dispatch(getPokemonData(name));
  }, [dispatch, name]);

  const renderPokemonInfo = () => {
    return isLoading === undefined || isLoading ? (
      <CircularProgress size={50} color="primary" />
    ) : (
      <PokemonDetailComponent pokemon={pokemon} />
    );
  };

  return <CardContainer small={true}>{renderPokemonInfo()}</CardContainer>;
};

export default PokemonDetail;
