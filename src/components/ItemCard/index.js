import React from "react";
import { Typography, Card, Grid } from "@material-ui/core";
import styles from "./style";

export const ItemCard = ({ name, url }) => {
  const classes = styles();
  return (
      <Card className={classes.itemCard}>
        <Grid className={classes.imageAndTitle}>
          <img src={`/items/${name}.png`} alt={name} />
          <Typography>{name}</Typography>
        </Grid>
      </Card>
  );
};

export default ItemCard;
