import {makeStyles} from '@material-ui/styles';

const centeredObj = {
    display: "flex",
    alignItems: "center",
    justifyContent:"center",
}

export default makeStyles({
    itemCard: {
        ...centeredObj,
        margin: "5px",
        minWidth: "150px",
    },
    imageAndTitle: {
        ...centeredObj,
        flexDirection: "column",
        width: "100%",
        textTransform: "capitalize"
    }
})