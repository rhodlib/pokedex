import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import Home from "./pages/Home";
import store from "./redux/store";
import PokemonList from "./pages/PokemonList";
import PokemonDetail from "./pages/PokemonDetail";
import BerriesList from "./pages/BerriesList";
import BerryDetail from "./pages/BerryDetail";
import ItemList from "./pages/ItemList";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/pokemons">
            <PokemonList />
          </Route>
          <Route exact path="/pokemon/:name">
            <PokemonDetail />
          </Route>
          <Route exact path="/berries">
            <BerriesList />
          </Route>
          <Route exact path="/berry/:name">
            <BerryDetail />
          </Route>
          <Route exact path="/items">
            <ItemList />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
