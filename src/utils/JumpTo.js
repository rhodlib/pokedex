import { useHistory } from 'react-router-dom';

export const JumpTo = rute => {
    let history = useHistory();
    return () => history.push(rute);
}

export default JumpTo;