import pokedex from './pokedex.png';
import berry from './berry.png';
import pokelogo from './Poke_logo.png';
import bag from './bag.png';

const assets = {
    pokedex,
    berry,
    pokelogo,
    bag
};

export default assets;